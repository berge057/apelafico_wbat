clear all
close all
clc

mainPath = pwd;
dataPath = fullfile(mainPath,'data');
dataSetPath = fullfile(dataPath,'2021-BE');

fileMat = dir(fullfile(dataSetPath,'*.json'));

% initialize array
varTypes = ["string","double","string","string","string","string","string","string","double","double","double","double","double","double","double","double","double","double","double","double","double","double","double","double","double"];
varNames = ["ID", ...
            "year", ...
            "dataSet", ...
            "stationName", ...
            "phaseName", ...
            "fileName", ...
            "scrutiny", ...
            "timeStart", ...
            "timeEnd", ...
            "lat", ...
            "lon", ...
            "circumference", ...
            "height", ...
            "frequency", ...
            "sampleCount", ...
            "svKurtosis", ...
            "svMax", ...
            "svMean", ...
            "svMeanTruncated05", ...
            "svMeanTruncated10", ...
            "svMeanTruncated25", ...
            "svMedian", ...
            "svMin", ...
            "svSkewness", ...
            "svVariance"];
        
paramsTab = table('Size',[0 length(varNames)],'VariableTypes',varTypes,'VariableNames',varNames);

overviewTab = readtable(fullfile(dataPath,'APELAFICO_overview.csv'),'Delimiter',',','Format','%d%s%s%s%f%f%s%s');

for idxFile = 1:length(fileMat)
    fileMat(idxFile).name
    fname = fullfile(fileMat(idxFile).folder,fileMat(idxFile).name); 
    fid = fopen(fname); 
    raw = fread(fid,inf); 
    str = char(raw');
    fclose(fid); 
    val = jsondecode(str);
    
    strTemp = strsplit(fileMat(idxFile).name,'.json');
    strTemp = strsplit(char(strTemp(1)),'_');
    
    myDataSet       = strTemp(1);
    mystationName   = strTemp(2);
    myPhaseName     = strTemp(3);
    myYear          = strsplit(char(strTemp(1)),'-');
    myYear          = str2double(myYear(1));
    
    paramsTabTemp = table('Size',[length(val.schools) length(varNames)],'VariableTypes',varTypes,'VariableNames',varNames);
	
    paramsTabTemp.ID                = vertcat(val.schools.objectNumber);
    paramsTabTemp.year              = repmat(myYear,length(val.schools),1);
	paramsTabTemp.dataSet           = repmat(myDataSet,length(val.schools),1);
    paramsTabTemp.stationName       = repmat(mystationName,length(val.schools),1);
    paramsTabTemp.phaseName         = repmat(myPhaseName,length(val.schools),1);
	paramsTabTemp.fileName          = {val.schools.fileName}';
	paramsTabTemp.timeStart         = {val.schools.timeStart}';
	paramsTabTemp.timeEnd           = {val.schools.timeEnd}';
	paramsTabTemp.circumference     = vertcat(val.schools.circumference);
	paramsTabTemp.height            = vertcat(val.schools.height);
    
    idxOverview = strcmp(   overviewTab.stationName,mystationName) & ...
                    strcmp(   overviewTab.phaseName,myPhaseName) & ...
                    strcmp(overviewTab.phaseName,myPhaseName);
    
    for idxSchool = 1:length(val.schools)
        paramsTabTemp.lat(idxSchool)                    = overviewTab.lat(idxOverview);
        paramsTabTemp.lon(idxSchool)                    = overviewTab.lon(idxOverview);
        paramsTabTemp.scrutiny(idxSchool)               = val.schools(idxSchool).scrutiny.restCategories;
        paramsTabTemp.frequency(idxSchool)              = val.schools(idxSchool).channels(1).frequency;
        paramsTabTemp.maxDepth(idxSchool)               = 35-val.schools(idxSchool).maxDepth;
        paramsTabTemp.minDepth(idxSchool)               = 35-val.schools(idxSchool).minDepth;
        paramsTabTemp.depth(idxSchool)                  = mean([paramsTabTemp.maxDepth(idxSchool) paramsTabTemp.minDepth(idxSchool)]);
        paramsTabTemp.sampleCount(idxSchool)            = val.schools(idxSchool).channels(1).sampleCount;
        paramsTabTemp.svKurtosis(idxSchool)             = val.schools(idxSchool).channels(1).svKurtosis;
        paramsTabTemp.svMax(idxSchool)                  = val.schools(idxSchool).channels(1).svMax/4/pi/1852^2;
        paramsTabTemp.svMean(idxSchool)                 = val.schools(idxSchool).channels(1).svMean/4/pi/1852^2;
        paramsTabTemp.svMeanTruncated05(idxSchool)      = val.schools(idxSchool).channels(1).svMeanTruncated05/4/pi/1852^2;
        paramsTabTemp.svMeanTruncated10(idxSchool)      = val.schools(idxSchool).channels(1).svMeanTruncated10/4/pi/1852^2;
        paramsTabTemp.svMeanTruncated25(idxSchool)      = val.schools(idxSchool).channels(1).svMeanTruncated25/4/pi/1852^2;
        paramsTabTemp.svMedian(idxSchool)               = val.schools(idxSchool).channels(1).svMedian/4/pi/1852^2;
        paramsTabTemp.svMin(idxSchool)                  = val.schools(idxSchool).channels(1).svMin/4/pi/1852^2;
        paramsTabTemp.svSkewness(idxSchool)             = val.schools(idxSchool).channels(1).svSkewness;
        paramsTabTemp.svVariance(idxSchool)             = val.schools(idxSchool).channels(1).svVariance/(4*pi*1852^2)^2;
    end

    if idxFile == 1
        paramsTab = paramsTabTemp;
    else
        paramsTab = [paramsTab; paramsTabTemp];
    end
end

writetable(paramsTab,fullfile(dataSetPath,'APELAFICO_school_parameters.csv'),'Delimiter',',')
% writetable(paramsTab,fullfile('data','APELAFICO_school_parameters_temp.csv'),'Delimiter',',')

plot_SA <- function(  figurePath,
                      prefix,
                      SA_min,
                      SA_hour){
  # plotting
  png(file.path(figurePath,paste0(prefix,'_SA_timeSeries.png')), 
      width = 12*1, height = 12*1, units = "cm", res = 300, pointsize = 10)
  
  df.plot <- subset(SA_min,depth != 'all')
  sts <- boxplot.stats(df.plot$SA)$stats
  
  p <- ggplot(df.plot, aes(x=time_min, y=SA,col=depth))+
        geom_point()
  
  print(p)
  dev.off()
  
  png(file.path(figurePath,paste0(prefix,'_SA_hours_boxplot.png')),
      width = 12*1, height = 12*1, units = "cm", res = 300, pointsize = 10)
  
  df.plot <- subset(SA_min,depth == 'all')
  sts <- boxplot.stats(df.plot$SA)$stats

  p <- ggplot(df.plot, aes(x=as.factor(hour), y=SA,col=as.factor(hour))) + 
        theme_classic()+
        geom_boxplot(outlier.colour=NA)+
        coord_cartesian(ylim = c(sts[2]/2,max(sts)*1.05))+
        theme(axis.text.x = element_text(angle = 90),legend.position = "none")+
        labs(x='Hour of day',y='SA')
  
  print(p)
  dev.off()

  png(file.path(figurePath,paste0(prefix,'_SA_depth.png')), 
      width = 12*1, height = 12*1, units = "cm", res = 300, pointsize = 10)
  
  df.plot <- subset(SA_min,depth != 'all')
  sts <- boxplot.stats(df.plot$SA)$stats
  
  p <- ggplot(df.plot, aes(x=as.factor(depth), y=SA)) + 
        theme_classic()+
        geom_boxplot(outlier.colour=NA,position=position_dodge(1))+
        coord_cartesian(ylim = c(sts[2]/2,max(sts)*1.05))+
        theme(axis.text.x = element_text(angle = 90),legend.position = "none")+
        labs(x='Depth channels',y='SA')
  
  print(p)
  dev.off()
}